package com.trustrace.SpringBatch.DbToDisplay;

import com.trustrace.SpringBatch.model.DisplayResultDto;
import com.trustrace.SpringBatch.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.text.ParseException;


@Component("DbToDisplayProcessor")
public class Processor implements ItemProcessor<User, DisplayResultDto> {

    @Override
    public DisplayResultDto process(User user) throws Exception {
        System.out.println("=====================Processsssssssss");
        return processing(user);
    }

    public DisplayResultDto processing(User user) throws ParseException {


        Float profit =  Float.parseFloat(user.getUnitPrice()+"")-Float.parseFloat(user.getUnitCost()+"");

        return new DisplayResultDto(user.getId(),user.getOrderID(),
                user.getRegion(),
                user.getCountry(),
                user.getItemType(), user.getSalesChannel(), user.getOrderPriority()
                ,profit,user.getTotalProfit());
    }
}
