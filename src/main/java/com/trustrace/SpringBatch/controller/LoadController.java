package com.trustrace.SpringBatch.controller;

import com.trustrace.SpringBatch.model.User;
import com.trustrace.SpringBatch.repository.UserRespository;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import javax.servlet.http.HttpServletRequest;

import java.io.*;
import java.nio.file.Files;

import java.nio.file.Paths;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.springframework.http.ResponseEntity.status;

@RestController
public class LoadController {
   public static String templateType;

    private static final Map<String, String> ORDER_PRIORITY =
            new HashMap<>();

    public LoadController() {
        ORDER_PRIORITY.put("Medium","M");
        ORDER_PRIORITY.put("Slow","C");
        ORDER_PRIORITY.put("Fast","H");
        ORDER_PRIORITY.put("UltraFast","L");
    }

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    @Qualifier("CsvToDb")
    Job CsvToDb;

    @Autowired
    @Qualifier("DbToDisplay")
    Job DbToDisplay;

    @Autowired
    UserRespository userRespository;



    @GetMapping("/load")
    public BatchStatus load() throws JobParametersInvalidException, JobExecutionAlreadyRunningException, JobRestartException, JobInstanceAlreadyCompleteException {


        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(CsvToDb, parameters);

        System.out.println("JobExecution: " + jobExecution.getStatus());
        System.out.println("Job Id: "+jobExecution.getJobId());
        System.out.println("Job Instance: "+jobExecution.getJobInstance());
        System.out.println(jobExecution.isStopping());
        System.out.println("End time: "+jobExecution.getEndTime());
        System.out.println("Batch is Running");
        while (jobExecution.isRunning()) {
            System.out.println("...");
        }

        return jobExecution.getStatus();
    }


    @GetMapping("/download")
    public ResponseEntity<InputStreamResource> dowmload(@RequestParam Optional<String> template, HttpServletRequest request) throws JobInstanceAlreadyCompleteException, JobExecutionAlreadyRunningException, JobParametersInvalidException, JobRestartException, FileNotFoundException {


        templateType=template.orElse("default");

        Map<String, JobParameter> maps = new HashMap<>();
        maps.put("time", new JobParameter(System.currentTimeMillis()));
        JobParameters parameters = new JobParameters(maps);
        JobExecution jobExecution = jobLauncher.run(DbToDisplay, parameters);

        System.out.println("JobExecution: " + jobExecution.getStatus());
        System.out.println("Job Id: "+jobExecution.getJobId());
        System.out.println("Job Instance: "+jobExecution.getJobInstance());
        System.out.println(jobExecution.isStopping());
        System.out.println("End time: "+jobExecution.getEndTime());
        System.out.println("Batch is Running");
        while (jobExecution.isRunning()) {
            System.out.println("...");
        }

        String filename = "src/main/resources/output.csv";

        try {
            File file = new File(filename);
            String zipFileName = "outputCsvFile.zip";

            FileOutputStream fos = new FileOutputStream(zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);

            zos.putNextEntry(new ZipEntry(file.getName()));

            byte[] bytes = Files.readAllBytes(Paths.get(filename));
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();
            zos.close();
        }

        catch(Exception e){
            System.out.println(e.getMessage());
        }


        String filename1 = "/home/hxtreme/dddd/SpringBatch (1)/SpringBatch/outputCsvFile.zip";

        InputStreamResource file = new InputStreamResource((new FileInputStream(filename1)));


        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=outputCsvFile.zip" )
                .contentType(MediaType.parseMediaType("application/zip"))
                .body(file);
    }
    @GetMapping("/post")
    public ResponseEntity<?> getAllPosts() throws IOException {

        int j = 0;
        int k =0 ;
       // System.out.println("post22222222222Controller");

        List<User> user = userRespository.findAll();
        for(User user1 : user){
            System.out.println(j+++"============jjj");
            String Code = user1.getOrderPriority();
            String priority = ORDER_PRIORITY.get(Code);
            user1.setOrderPriority(priority);
        }

        FileWriter csvWriter = new FileWriter("src/main/resources/output.csv");
        csvWriter.append("No");
        csvWriter.append(",");
        csvWriter.append("Region");
        csvWriter.append(",");
        csvWriter.append("Country");
        csvWriter.append(",");
        csvWriter.append("item Type");
        csvWriter.append(",");
        csvWriter.append("Sales Channel");
        csvWriter.append(",");
        csvWriter.append("Order Priority");
        csvWriter.append(",");
        csvWriter.append("Order ID");
        csvWriter.append(",");
        csvWriter.append("Total Profit");
        csvWriter.append("\n");

        for (User rowData : user) {
            //System.out.println("=============outputtWritinggggggggggg");
            //System.out.println("========================"+k++);
            csvWriter.append(k+++",");
            csvWriter.append(rowData.getRegion()+",");
            csvWriter.append(rowData.getCountry()+",");
            csvWriter.append( rowData.getItemType()+",");
            csvWriter.append( rowData.getSalesChannel()+",");
            csvWriter.append( rowData.getOrderPriority()+",");
            csvWriter.append( rowData.getOrderID()+",");

            csvWriter.append( rowData.getTotalProfit()+"");
            csvWriter.append("\n");
        }
        csvWriter.flush();
        csvWriter.close();
        System.out.println("Finished");
        return status(HttpStatus.OK).body( userRespository.count());
    }

}
