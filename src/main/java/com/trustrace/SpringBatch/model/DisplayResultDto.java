package com.trustrace.SpringBatch.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DisplayResultDto {

    @Id
            Long id;
    Integer orderID;
    String region;
    String  country;
    String itemType;
    String  salesChannel;
    String  orderPriority;
    Float  Profit;
    String  totalProfit;

}
