package com.trustrace.SpringBatch.DbToDisplay;

import com.trustrace.SpringBatch.model.User;
import com.trustrace.SpringBatch.repository.UserRespository;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("DbToDisplayReader")
public class CustomItemReader implements ItemReader<User> {

    @Autowired
    UserRespository userRespository;
    public static  long index=1,lastindex;
    public CustomItemReader() {
        //index=1;
       // lastindex = 1000000; //userRespository.count();
    }
    @Override
    public User read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
        User nextUser = null;
        //System.out.println(index+"==============indexindex");
        if(index==1){
           System.out.println("oneTymmm");
            lastindex= userRespository.count();
       }

        if(index<=lastindex){
            nextUser = userRespository.findById((int) index).get();
            index++;
        } else {
            System.out.println("Exittttttttttttt");
            index=1;
        }
        return nextUser;
    }
}