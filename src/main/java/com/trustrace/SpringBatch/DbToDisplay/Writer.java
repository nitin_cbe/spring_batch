package com.trustrace.SpringBatch.DbToDisplay;

import com.trustrace.SpringBatch.abstractProcess.Exceltemplate;
import com.trustrace.SpringBatch.controller.LoadController;
import com.trustrace.SpringBatch.model.DisplayResultDto;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;
@Component("DbToDisplayWriter")
public class Writer implements ItemWriter<DisplayResultDto> {

    @Autowired
    @Qualifier("country")
    Exceltemplate excelTemplatecountry;

    @Autowired
    @Qualifier("region")
    Exceltemplate excelTemplateregion;

    @Autowired
    @Qualifier("default")
    Exceltemplate excelTemplaterdefault;


    @Override
    public void write(List<? extends DisplayResultDto> list) throws Exception {

        LoadController ll = new LoadController();
        String template = ll.templateType;

              if(template.equals("country")){
                  excelTemplatecountry.writeExcel(list);
              }

             if(template.equals("region")){
                  excelTemplateregion.writeExcel(list);
             }

        if(template.equals("default")){
            excelTemplaterdefault.writeExcel(list);
        }

    }
}