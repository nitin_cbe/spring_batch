package com.trustrace.SpringBatch.repository;

import com.trustrace.SpringBatch.model.User;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface UserRespository extends MongoRepository<User,Integer> {


}
