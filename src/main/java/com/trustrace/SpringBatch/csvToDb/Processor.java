package com.trustrace.SpringBatch.csvToDb;

import com.trustrace.SpringBatch.model.User;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component("CsvToDbProcessor")
public class Processor implements ItemProcessor<User,User>{

    private static final Map<String, String> ORDER_PRIORITY =
            new HashMap<>();

    public Processor() {
        ORDER_PRIORITY.put("M","Medium");
        ORDER_PRIORITY.put("C","Slow");
        ORDER_PRIORITY.put("H","Fast");
        ORDER_PRIORITY.put("L","UltraFast");
    }

    @Override
    public User process(User user) throws Exception {
        String Code = user.getOrderPriority();
        String priority = ORDER_PRIORITY.get(Code);
        user.setOrderPriority(priority);
        System.out.printf("Converted from [%s] to [%s]%n", Code, priority);
        return user;
    }
}
