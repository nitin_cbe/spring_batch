package com.trustrace.SpringBatch.abstractProcess;

import com.trustrace.SpringBatch.model.DisplayResultDto;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public abstract class Exceltemplate {

    public Exceltemplate(){
        super();
    }

    public abstract void writeExcel(List<? extends DisplayResultDto> list) throws IOException;

//    public void defaultWriteExcel(List<? extends DisplayResultDto> list) throws IOException {
//
//        System.out.println("workkk avuthuuuu");
//    }


}
