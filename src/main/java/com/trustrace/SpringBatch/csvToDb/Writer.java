package com.trustrace.SpringBatch.csvToDb;

import com.trustrace.SpringBatch.model.User;
import com.trustrace.SpringBatch.repository.UserRespository;
import com.trustrace.SpringBatch.service.SequenceGeneratedService;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("CsvToDbWriter")
public class Writer  implements ItemWriter<User>{

    @Autowired
    UserRespository userRespository;

    @Autowired
    SequenceGeneratedService sequenceGeneratorService;

    @Override
    public void write(List<? extends User> users) throws Exception {

       for(User user: users){
           user.setId(sequenceGeneratorService.generateSequence(User.SEQUENCE_NAME));

       }
        userRespository.saveAll(users);
    }
}
