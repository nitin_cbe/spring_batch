package com.trustrace.SpringBatch.abstractProcess;

import com.trustrace.SpringBatch.DbToDisplay.CustomItemReader;
import com.trustrace.SpringBatch.model.DisplayResultDto;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

@Component("default")
public class DefaultType extends Exceltemplate {


    List<? extends DisplayResultDto> writeList;
    int counter =1;
    int flag =0;
    FileWriter csvWriter;
    Long dbCount;

    public DefaultType(){


    }


    @Override
    public void writeExcel(List<? extends DisplayResultDto> list) throws IOException {
        CustomItemReader customItemReader = new CustomItemReader();
        writeList=list;
        if(flag==0) {

            dbCount=  customItemReader.lastindex;
            //System.out.println(dbCount+"=============dbCountdbCountdbCount");
            csvWriter = new FileWriter("src/main/resources/output.csv");
            csvWriter.append("No");
            csvWriter.append(",");
            csvWriter.append("Region");
            csvWriter.append(",");
            csvWriter.append("Country");
            csvWriter.append(",");
            csvWriter.append("item Type");
            csvWriter.append(",");
            csvWriter.append("Sales Channel");
            csvWriter.append(",");
            csvWriter.append("Order Priority");
            csvWriter.append(",");
            csvWriter.append("Order ID");
            csvWriter.append(",");
            csvWriter.append("Profit");
            csvWriter.append(",");
            csvWriter.append("Total Profit");
            csvWriter.append("\n");
        }

        for (DisplayResultDto rowData : writeList) {
            System.out.println("----------"+ counter +"-------Writerrrrrrrrrrrrrr"+rowData.getCountry());

            csvWriter.append(counter+++",");
            csvWriter.append(rowData.getRegion()+",");
            csvWriter.append(rowData.getCountry()+",");
            csvWriter.append( rowData.getItemType()+",");
            csvWriter.append( rowData.getSalesChannel()+",");
            csvWriter.append( rowData.getOrderPriority()+",");
            csvWriter.append( rowData.getOrderID()+",");
            csvWriter.append( rowData.getProfit()+",");
            csvWriter.append( rowData.getTotalProfit()+"");
            csvWriter.append("\n");
            flag++;
            if (flag == dbCount) {
                System.out.println(flag+"==============33333333333111111111");
                flag = 0;
                counter = 1;
                csvWriter.close();
                System.out.println("Excel file has been generated successfully.");
            }
        }

    }
}
